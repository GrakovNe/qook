package org.grakovne.qook.engine.listeners;

import java.util.EventListener;

public interface LevelCompleteListener extends EventListener {
    void levelComplete();
}
