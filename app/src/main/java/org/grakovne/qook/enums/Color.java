package org.grakovne.qook.enums;

public enum Color {
    BLACK,
    GRAY,
    GREEN,
    RED,
    BLUE,
    YELLOW,
    PURPLE,
    CYAN
}
