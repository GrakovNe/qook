package org.grakovne.qook.enums;

public enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN,
    NOWHERE
}
