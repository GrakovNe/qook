package org.grakovne.qook.enums;

public enum GameExceptionCodes {
    INCORRECT_LEVEL,
    HISTORY_IS_EMPTY
}
