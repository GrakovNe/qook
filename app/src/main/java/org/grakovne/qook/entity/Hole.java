package org.grakovne.qook.entity;

import org.grakovne.qook.enums.Color;

public class Hole extends Item {
    public Hole(Color color) {
        super(color);
    }
}
