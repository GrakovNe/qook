package org.grakovne.qook.entity;

import org.grakovne.qook.enums.Color;

public class Block extends Item {
    public Block() {
        super(Color.GRAY);
    }
}
