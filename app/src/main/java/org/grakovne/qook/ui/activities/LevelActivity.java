package org.grakovne.qook.ui.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.grakovne.qook.R;
import org.grakovne.qook.engine.Field;
import org.grakovne.qook.engine.Level;
import org.grakovne.qook.engine.listeners.FieldUpdatingListener;
import org.grakovne.qook.engine.listeners.HistoryStatesListener;
import org.grakovne.qook.engine.listeners.LevelCompleteListener;
import org.grakovne.qook.entity.Item;
import org.grakovne.qook.exceptions.GameException;
import org.grakovne.qook.managers.HistoryManager;
import org.grakovne.qook.managers.LevelManager;
import org.grakovne.qook.managers.SharedSettingsManager;
import org.grakovne.qook.ui.views.FieldView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static android.view.View.OnTouchListener;

public class LevelActivity extends BaseActivity {
    @InjectView(R.id.field)
    FieldView fieldView;
    @InjectView(R.id.reset_level_button)
    ImageButton resetLevelButton;
    @InjectView(R.id.level_counter)
    TextView levelCounter;
    @InjectView(R.id.back_level_button)
    ImageButton backLevelButton;
    @InjectView(R.id.undo_step_button)
    ImageButton undoStepButton;
    @InjectView(R.id.game_activity)
    LinearLayout gameActivity;

    private int currentLevelNumber;

    private Bundle savedData;

    private LevelManager levelManager = null;
    private Level level = null;

    private float downHorizontal;
    private float downVertical;
    private float upHorizontal;
    private float upVertical;

    private Handler handler;

    private Timer timer;
    private TimerTask task;

    private Animation animation;
    private SharedSettingsManager sharedSettingsManager = SharedSettingsManager.build(this);
    private HistoryManager historyManager;

    private Runnable invalidateView = new Runnable() {
        @Override
        public void run() {
            fieldView.invalidate();
        }
    };

    private static final int FRAME_DELAY = 1;

    private static List<Item[][]> levels = new ArrayList<>();

    private OnTouchListener onFieldTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    downHorizontal = event.getX();
                    downVertical = event.getY();
                    break;

                case MotionEvent.ACTION_UP:
                    upHorizontal = event.getX();
                    upVertical = event.getY();

                    fieldView.setClickable(false);

                    fieldView.getField().makeTurn(
                            fieldView.getElementCoordinates(downHorizontal, downVertical),
                            fieldView.getSwipeDirection(downHorizontal, upHorizontal, downVertical, upVertical)
                    );

                    fieldView.setClickable(false);

            }
            return true;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        ButterKnife.inject(this);

        handler = new Handler();
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.field_changes_anim);

        levelManager = LevelManager.build(this);
        fieldView.setOnTouchListener(onFieldTouchListener);

        savedData = savedInstanceState;

        int levelNumber = getIntent().getIntExtra(DESIRED_LEVEL, 1);

        if (savedData != null && savedData.getInt(LEVEL_NUMBER) == levelNumber) {
            restoreLevel();
        } else {
            openLevel(levelNumber);
        }

        currentLevelNumber = levelNumber;


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    private void setListeners(final Field field) {

        field.setUpdatingListener(new FieldUpdatingListener() {
            @Override
            public void startUpdate() {
                timer = new Timer();
                task = new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(invalidateView);
                    }
                };

                timer.scheduleAtFixedRate(task, 0, FRAME_DELAY);

                historyManager.saveToHistory(fieldView.getField().getLevel());
            }

            @Override
            public void finishUpdate() {
                timer.cancel();
                handler.post(invalidateView);
            }
        });

        field.setCompleteListener(new LevelCompleteListener() {
            @Override
            public void levelComplete() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        fieldView.invalidate();
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        animateView(fieldView);

                        try {
                            levelManager.finishLevel();
                            openLevel(levelManager.getCurrentLevelNumber());
                            getIntent().putExtra(DESIRED_LEVEL, levelManager.getCurrentLevelNumber());
                            fieldView.layout(0, 0, 0, 0);
                        } catch (GameException ex) {
                            onMenuClick();
                        }
                    }
                });
            }
        });
    }

    private void openLevel(int levelNumber) {
        try {
            level = levelManager.getLevel(levelNumber);
            final Field field = new Field(level, sharedSettingsManager.isAnimationNeed());

            setListeners(field);

            fieldView.setField(field);
            setLevelCounterText(levelNumber);
            currentLevelNumber = levelNumber;

            if (historyManager != null) {
                historyManager.clearHistory();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void restoreLevel() {
        Field field = (Field) savedData.getSerializable(FIELD);
        setListeners(field);
        fieldView.setField(field);
        setLevelCounterText(savedData.getInt(LEVEL_NUMBER));
    }

    private void setLevelCounterText(int levelNumber) {
        StringBuilder builder = new StringBuilder();

        builder
                .append(String.format(Locale.getDefault(), "%02d", levelNumber))
                .append(" / ")
                .append(String.format(Locale.getDefault(), "%02d", levelManager.getLevelsNumber()));

        levelCounter.setText(builder.toString());
    }

    private void animateView(View view) {
        view.startAnimation(animation);
    }

    @Override
    public void onResume() {
        super.onResume();
        overridePendingTransition(0, 0);

        if (fieldView.getField() != null) {
            fieldView.getField().setIsAnimation(sharedSettingsManager.isAnimationNeed());
        }

        boolean isUndoAvailable = sharedSettingsManager.isUndoPurchased();

        if (!isUndoAvailable) {
            undoStepButton.setVisibility(View.GONE);
        } else {
            undoStepButton.setVisibility(View.VISIBLE);
        }


        historyManager = HistoryManager.build(new HistoryStatesListener() {
            @Override
            public void historyIsEmpty() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        undoStepButton.setEnabled(false);
                        undoStepButton.getBackground().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.dark_button), PorterDuff.Mode.MULTIPLY);
                    }
                });

            }

            @Override
            public void historyIsContainsSomething() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        undoStepButton.setEnabled(true);
                        undoStepButton.getBackground().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.normal_button), PorterDuff.Mode.MULTIPLY);
                    }
                });
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(FIELD, fieldView.getField());
        outState.putInt(LEVEL_NUMBER, currentLevelNumber);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    @OnClick(R.id.reset_level_button)
    public void onResetClick() {
        fieldView.layout(0, 0, 0, 0);
        openLevel(levelManager.getCurrentLevelNumber());
    }

    @OnClick(R.id.back_level_button)
    public void onMenuClick() {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @OnClick(R.id.undo_step_button)
    public void onUndoStepClick() {
        Level level = historyManager.getFromHistory();
        fieldView.getField().loadFromHistory(level);
        fieldView.invalidate();
    }
}